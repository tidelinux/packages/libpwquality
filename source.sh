# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/libpwquality/libpwquality/releases/download/libpwquality-${BPM_PKG_VERSION}/libpwquality-${BPM_PKG_VERSION}.tar.bz2"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr                      \
              --disable-static                   \
              --with-securedir=/usr/lib/security \
              --disable-python-bindings
  make
  pip3 wheel -w dist --no-build-isolation --no-deps --no-cache-dir $PWD/python
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  pip3 install --root="$BPM_OUTPUT" --ignore-installed --no-index --find-links=dist --no-cache-dir --no-user pwquality
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/libpwquality
  cp COPYING "$BPM_OUTPUT"/usr/share/licenses/libpwquality/COPYING
}
